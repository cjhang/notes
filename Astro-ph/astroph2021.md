[toc]

# Astro-ph in 2021

## Driven galactic outflows with magnetic fields

Steinwandel et al. [arXiv:2012.12905](https://arxiv.org/abs/2012.12905)

![image-20210104212845999](astroph2021.assets/image-20210104212845999.png)

Galactic outflows are ubiquitous in galaxies. They can be powered by stellar feedback or more energetic AGN feedback, but the exact way to lunch the outflows is still far from being understood. This work proposed a toy model to link the local magnetic with the outflow. To make magnetic strong enough to lead galactic outflows, the crutial question is what is the magnetic seed and how they can be amplified.

The ways to form the magnetic seed field including: mis-alignment of pressure and density gradients(Biermann-battery), Slow down of photons from Thompson scattering, supernovae (𝛼-Ω-dynamo), and plasma instability (Weibel-Instability). This work more concentrated on the amplication dynamo, which they called $𝛼^2$-Ω-dynamo, which featured that the small scale magnetic field can be amplified exponentially by small-scale turbulent. In a Milky Way like disk galaxy, the small scale magnetic field can be amplified by the formation of bar, then the strong magnetic field can launch galactic wind.



## Non-thermal filaments from the tidal destruction of clouds in Galactic center

Coughlin et al. [arXiv:2010.13790](https://arxiv.org/abs/2010.13790). [MNRAS](https://academic.oup.com/mnras/article/501/2/1868/6027702?rss=1)

![image-20210104200443387](astroph2021.assets/image-20210104200443387.png)

Many non-thermal filaments were found in the Galactic center, with different length and different orientation. However, the longest ones were found to be perpendicular to the Galactic disk and their magnetic is highly ordered (10𝜇G to 1mG) and parallel to its axis. The formation of these kind of filaments has long been a puzzle, though several ways have been  proposed, like shock, stellar wind, outflow.

In this work, the authors have proposed one simple channel to form this magnetized filaments. The figure nicely summarrized their model. When the molecular clouds approach the SMBH (or central stellar cluster), the gravitational force will stretch the clouds in radical direction. At the same time, the closed magnetic field in the clouds will also be stretch along the same direction, and the following magnetic reconnection will make the ordered magnetic field parallel to the axis.

This is a theoretical paper, but the text and the sketch is quite easy to understand. Besides this small scale phenomena in Galactic center, the kinematics induced magnetic field could be also interesting in galactic interactions. If so, we may also expect to find the large scale ordered magnectic field in the tidal distrupted satellites. Beside, it may be important for high-energy physics, since the magnetic reconnection can play some roles here.