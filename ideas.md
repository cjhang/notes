# Science Projects

## ALMACAL

ALMACAL is the ALMA ancillary project, which collects all the observations of the ALMA calibrators. 

1. ~~ALMACAL time domain data, can be used to trace the gas filaments fast motion in the early Universe?~~

   > - the residual of the central point source may dominate the noise, which will create some fake variational signal.

2. Find the proto-group in deep small field observation? Galaxy group is smaller than galaxy cluster, so ALMACAL can be used to search for proto-groups in the early Universe.

## Early Universe

1. There is initial mass function for stars, how about the initial mass function for galaxies? Will that matters for the proto-cluster formation or can it be the characteristics to distinguish different evolution stage of clusters? Any possible for the initial mass function of black whole seed at the reionization era?

2. **Determine the kinematics of [C II] and the other gas phase tracer**

   > The [C II] has been widely used to study the kinematics or even the dynamics of high-z galaxies. However, little researchs have been focused on their validity. The FIEA on SOFIA can be the best instrument to study the [C II] kinematics of local spiral or starburst galaxies.
   >
   > The target can also be some metal-poor or low-ionization dwarf
   >
   > **Using simalma to test the spatial resolution effect on the interpretation of the kinematics from [C II]**

3. Study the role of star formation efficiency in spiral. Any enhancement?

   > This thought comes out when I read the study about the Milky Way's spiral arms. They did not find significant strengthen of SFE in the spiral arms. They argued the large star formation surface density is mainly because the higher efficiency of converting atomic hydrogen to molecular gas.
   >
   > Another crazy idea: it is possible the molecular gas are also not formed in the spiral arms, but only due to some dynamic friction+gravitational contraction?

4. Find MW analoges in the early Universe

   > SPT0418-47 galaxy is thought to be alike MK from their kinematic signature. How about the SED and the environment?

5. **Are high-z starburst galaxies are mostly regular rotation dominate?**

   > From current high resolution [C II] observations, it seems that most galaxies with strong [C II] are rotation dominated. Since most of these galaxies only barely resolved, so it definitely need the kinematics from other gas tracer. **Maybe one proposal for next ALMA cycle**

6. ~~Measuring the magnetic filed in the submillimeter bright galaxies and the Lyman-a galaxies in very high-z galaxies, additional evidence of the high turbulence?~~

   > The kinetic SZ effect and the synchrotron are both hard to measure at early Univere, but it definitely important for future surveys

7. Try to observe the magnetic at high-z galaxies.

   > The first ideal target could be the strong lensed source, they were bright and could reach a relative high angular resolution.
   >
   > - Red Radio Ring (Geach+2015)
   >
   > - [ ] read the review paper of magnetic field measures, and test the possibility to measure the magnetic field in high-z Universe

## AGN

1. Find timescale than AGN activity before  star formation, find evidence of AGN enhance star formation. Jet produce ring star formation, alpha/Fe abundance.
2. Gas replenish in the Elliptical galaxies, the frequency, the importance, the origin. High star formation efficiency in early type galaxies 

## Local galaxies

1. High resolution observation of local galaxies with low inclination angle, seperating out the weak outflow

   > It relies on the model subtraction to reomove the strong regular rotation, so that it where the uncertainty comes from.

2. DESI has much better coverage for close pair sample than SDSS, can be a much better sample to study galaxy merger. The advantages of DESI also in the deeper image, which can even be used for statistically study the minor merger.

3. Magnetic field in local interacting galaxies

   > It is a idea come from the study of magnetized filaments in Galactic center, [Coughlin et al.](https://arxiv.org/abs/2010.13790) have proposed that the tidal disruption may also play a role for the formation of the magnetic field. So it would be interesting to search the magnetic field in the tidal disrupted galaxies.

## Rotational curve

Study how the rotational curve changed from Cusp to Core:

1. From Ultra Diffused Galaxy -> Ultra Compact Galaxy, study the compactness of the dark matter? 

2. Maybe a parameter to quantify the compactness of dark matter is essential?

3. The compactness of dark matter as a function of galaxy type, stellar mass, baryonic compactness, age, redshift

4. High redshift SMG? If the feedback have not affect the distribution of DM

   > It may need the evidence about how long the stellar feedback can affect the distribution of dark matter.

5. Proto-cluster, if the all the members gathered in one dark matter halo, is it possible to measure the rotational curve of the cluster by the cluster members?

   > It may not be a good idea, if the proto-cluster cannot be regarded as one fully thermalized system



6. Study the rotational curve of the disk galaxy at high-z, from gravitational lensing? 

   > They may underwent fewer feedback than local galaxies, which can preserve their cusp dark matter profile?

## IMBH

1. IMBH has a variability scale of hours. IFU opportunity to search for off-nuclear IMBH, search for manga repeat observation and test the possibility of china space 2.5meter telescope. 

## Cosmology

- If the core and cusp problem favour the interaction between dark matter and the baryons, will this effect have any signals in the BAO?



## Technicals

Power Spectra Distribution:

1. Anything related with SSP fitting? 

   



# PhD life



## Knowledge needed for PhD

1. Bayesian inference

   > - [ ] Systematically study the Baysian inference with a fixed plan
   >
   > Maybe one software to model the kinematics based on Bayesian inference?
   >
   > http://arxiv.org/abs/2011.12023 can be a good reference.

2. SED fitting, CIGAL, MBB

3. Dust physics





# Questions

Questions to be questioned:

1. Why the interferometers has much accurate astrometry? Due to the phase calibrator?

   > Because the phase calibrator. The phase calibrator can have very accurate astrometry, in this case, we can also achieve very high accurate relative position to phase calibrator. As a result, we can achieve much higher position accurate for our science target.





MUSE observations of 37 3C low-z radio galaxies, overlap with three ALMA calibrator:

- 3c196.1
- 3c456
- 3c459
- 3c287 (possible)
- 3c327 (possible)





# Non-astronomy

1. Deep Learning: Learning the random number? How it works or whether it works. Learn real world random number and combining several random number generators.

